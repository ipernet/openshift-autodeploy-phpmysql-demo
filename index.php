<pre>
<?php

$db_name	=	getenv('DB_NAME');
$db_host	=	getenv('DB_HOST');
$db_user	=	getenv('DB_USER');
$db_pass	=	getenv('DB_PWD');

try
{
	$dbh = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_user, $db_pass);

	echo 'Connected to mysql://'.$db_user.'@'.$db_host.'/'.$db_name."\n";
	
	echo "\n";
	
	echo 'SELECT DATABASE() = ';
	$q	=	$dbh->prepare('SELECT DATABASE()');
	print_r($q->execute() ? $q->fetchAll(PDO::FETCH_ASSOC) : false);
	
	echo "\n";
	
	echo 'SHOW GRANTS = ';
	$q	=	$dbh->prepare('SHOW GRANTS');
	print_r($q->execute() ? $q->fetchAll(PDO::FETCH_ASSOC) : false);
	
	echo "\n";
	
	$dbh = null;
}
catch(PDOException $e)
{
	print_r($e);
}

echo '$_ENV = ';
print_r($_ENV);

echo "\n";

echo '$_SERVER = ';
print_r($_SERVER);

?>
</pre>
